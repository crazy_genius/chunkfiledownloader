package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type writeCounter struct {
	Total uint64
}

func (wc *writeCounter) Write(p []byte) (int, error) {
	n := len(p)
	wc.Total += uint64(n)
	wc.PrintProgress()

	return n, nil
}

func (wc writeCounter) PrintProgress() {
	fmt.Printf("\r%s", strings.Repeat(" ", 35))
	fmt.Printf("\rDownloading ... %d kb complete", wc.Total/1000)
}

type downloadRange struct {
	Start  int
	End    int
	Offset uint
	Data   []byte
}

func main() {

	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "usage: %s url\n", os.Args[0])
		os.Exit(1)
	}

	url := os.Args[1]
	fmt.Printf("Downloading %s...\n", url)

	if err := downloadFile(url); err != nil {
		fmt.Printf("Error accure %s...\n", err.Error())
		os.Exit(1)
	}

	fmt.Printf("Complete\n")
}

func downloadFile(url string) error {
	out, err := os.Create("/tmp/download.tmp")
	if err != nil {
		return err
	}

	defer out.Close()

	resp, err := http.Get(url)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	contentSize, _ := strconv.Atoi(resp.Header["Content-Length"][0])

	if err := out.Truncate(int64(contentSize)); err != nil {
		return err
	}

	if resp.Header["Accept-Ranges"][0] == "bytes" {
		chunckDownload(url, resp, out)

		return nil
	}

	if err := directDownload(resp, out); err != nil {
		return err
	}

	if err := os.Rename("/tmp/download.tmp", "./download.file"); err != nil {
		return err
	}

	return nil
}

func chunckDownload(url string, r *http.Response, f *os.File) error {
	chunkChan := make(chan downloadRange)
	defer close(chunkChan)
	done := make(chan bool, 1)
	defer close(done)
	contentSize, _ := strconv.Atoi(r.Header["Content-Length"][0])

	fmt.Printf("Contetnt size: %s", r.Header["Content-Length"][0])
	chunkSize := int(contentSize / 10)
	fmt.Printf("Chunk Size: %d", int(chunkSize))

	startByte := 0
	endByte := 0
	totalChunks := 10
	for i := 0; i < totalChunks; i++ {
		endByte = startByte + int(chunkSize)
		dr := downloadRange{
			Start: startByte,
			End:   endByte,
		}

		startByte = endByte

		go func(url string, dr downloadRange, drChan chan<- downloadRange) {
			fmt.Printf("Start download chunck: %d-%d \n", dr.Start, dr.End)
			client := new(http.Client)
			req, err := http.NewRequest("GET", url, nil)

			if err != nil {
				fmt.Printf("%s\n", err.Error())
				return
			}

			req.Header.Set("Range", fmt.Sprintf("bytes=%d-%d", dr.Start, dr.End-1))
			res, err := client.Do(req)
			if err != nil {
				fmt.Printf("%s\n", err.Error())
				return
			}
			defer res.Body.Close()
			counter := &writeCounter{0}
			var part bytes.Buffer
			partWriter := bufio.NewWriter(&part)
			if _, err := io.Copy(partWriter, io.TeeReader(res.Body, counter)); err != nil {
				fmt.Printf("%s\n", err.Error())
				return
			}

			fmt.Print("\n")

			drChan <- downloadRange{
				Start:  dr.Start,
				End:    dr.End,
				Offset: dr.Offset,
				Data:   part.Bytes(),
			}
			fmt.Printf("End download chunck: %d-%d \n", dr.Start, dr.End)
		}(url, dr, chunkChan)
	}

	var writedChunks int

	go func(writedChunks int, totalChunks int, f *os.File, dataFlow <-chan downloadRange, ctrl chan<- bool) {

		for {
			select {
			case r := <-dataFlow:
				writedChunks++
				fmt.Printf("Try to put chunck: %d-%d \n", r.Start, r.End)
				if _, err := f.Seek(int64(r.Start), 0); err != nil {
					fmt.Printf("What should i do \n")
				}

				f.Write(r.Data)
				fmt.Printf("Saved chunck: %d-%d \n", r.Start, r.End)
			}

			if writedChunks >= totalChunks {
				ctrl <- true
				break
			}
		}

	}(writedChunks, totalChunks, f, chunkChan, done)

	<-done

	if _, err := f.Seek(0, 0); err != nil {
		fmt.Printf("What should i do \n")
	}

	return nil
}

func directDownload(r *http.Response, f *os.File) error {
	counter := &writeCounter{0}
	if _, err := io.Copy(f, io.TeeReader(r.Body, counter)); err != nil {
		return err
	}

	fmt.Print("\n")

	return nil
}
